package br.com.alura.leilao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.alura.leilao.exception.LanceMenorQueUltimoLanceException;
import br.com.alura.leilao.exception.LanceSeguidoDoMesmoUsuarioException;
import br.com.alura.leilao.exception.UsuarioJaDeuCincoLancesException;
import br.com.alura.leilao.util.MoedaUtil;

public class Leilao implements Serializable {

    private final String descricao;
    private final List<Lance> lances;
    private Double maiorLance = 0.0;
    private Double menorLance = 0.0;

    public Leilao(String descricao) {
        this.descricao = descricao;
        this.lances = new ArrayList<>();
    }

    public void propoe (Lance lance){
        valida(lance);
        lances.add(lance);
        double valorLance = lance.getValor();
        if (defineMaiorEMenorLanceParaPrimeiroLance(valorLance)) return;
        Collections.sort(lances);
        calculaMaiorLance(valorLance);
        //calculaMenorLance(valorLance);
    }

    private boolean defineMaiorEMenorLanceParaPrimeiroLance(double valorLance) {
        if (lances.size() == 1){
            maiorLance = valorLance;
            menorLance = valorLance;
            return true;
        }
        return false;
    }

    private void valida(Lance lance) {
        double valorLance = lance.getValor();
        if (lanceForMenorQueUltimoLance(valorLance)) throw new LanceMenorQueUltimoLanceException(); //throw new RuntimeException("Lance foi menor que maior lance");
        if(temLances()){
           Usuario usuaruiNovo = lance.getUsuario();
            if (usuarioForOMesmoDoUltimoLance(usuaruiNovo)) throw new LanceSeguidoDoMesmoUsuarioException();//throw new RuntimeException("Mesmo usuario do ultimo lance");

            if (UsuarioDeuCincoLances(usuaruiNovo)) throw new UsuarioJaDeuCincoLancesException();//throw new RuntimeException("Usuario ja deu cinco lances");
        }
        //return false;
    }

    private boolean temLances() {
        return !lances.isEmpty();
    }

    private boolean UsuarioDeuCincoLances(Usuario usuaruiNovo) {
        int lancesDoUsuario = 0;
        for (Lance l:
             lances) {
            Usuario usuarioExistente = l.getUsuario();
            if(usuarioExistente.equals(usuaruiNovo)){
                lancesDoUsuario++;
                if(lancesDoUsuario == 5){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean usuarioForOMesmoDoUltimoLance(Usuario usuaruiNovo) {
        Usuario Ultimousuario = lances.get(0).getUsuario();
        if(usuaruiNovo.equals(Ultimousuario)){
            return true;
        }
        return false;
    }

    private boolean lanceForMenorQueUltimoLance(double valorLance) {
        if(maiorLance > valorLance){
            return true;
        }
        return false;
    }

    private void calculaMenorLance(double valorLance) {
        if(valorLance < menorLance){
            menorLance = valorLance;
        }
    }

    private void calculaMaiorLance(double valorLance) {
        if(valorLance > maiorLance){
            maiorLance = valorLance;
        }
    }

    public Double getMenorLance() {
        return menorLance;

    }

    public Double getMaiorLance() {
        return maiorLance;
    }

    public String getDescricao() {
        return descricao;
    }

    public List<Lance> tresMaioresLances() {
        int quantidadesMaximaLances = lances.size();
        if(quantidadesMaximaLances > 3){
            quantidadesMaximaLances = 3;
        }
        return lances.subList(0, quantidadesMaximaLances);
    }

    public int quantidadeLances() {
        return lances.size();
    }
}
