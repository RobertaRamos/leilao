package br.com.alura.leilao.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import br.com.alura.leilao.R;
import br.com.alura.leilao.model.Lance;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.util.MoedaUtil;

public class LancesLeilaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lances_leilao);
        Intent dadosRecebidos = getIntent();
        if(dadosRecebidos.hasExtra("leilao")){
            Leilao leilao = (Leilao) dadosRecebidos.getSerializableExtra("leilao");
            mostraDescricao(leilao);
            mostraMaiorLance(leilao);
            mostraMenorLance(leilao);
            mostraMaioresLances(leilao);
        }
    }

    private void mostraMaioresLances(Leilao leilao) {
        TextView maioresLances = findViewById(R.id.lances_leilao_maiores_lances);
        StringBuilder sb = new StringBuilder();
        for(Lance lance : leilao.tresMaioresLances()){
            String maioresLancesOfertador = MoedaUtil.formataMoedaParaBrasileiro(lance.getValor());
            sb.append(maioresLancesOfertador + "\n");
        }
        //String maioresLanecsEmTexto = sb.toString();
        maioresLances.setText(sb);
    }

    private void mostraMenorLance(Leilao leilao) {
        TextView menorLance = findViewById(R.id.lances_leilao_menor_lance);
        String menorLanceOfertado = MoedaUtil.formataMoedaParaBrasileiro(leilao.getMenorLance());
        menorLance.setText(menorLanceOfertado);
    }

    private void mostraMaiorLance(Leilao leilao) {
        TextView maiorlance = findViewById(R.id.lances_leilao_maior_lance);
        String maiorLanceOfertado = MoedaUtil.formataMoedaParaBrasileiro(leilao.getMaiorLance());
        maiorlance.setText(maiorLanceOfertado);
    }

    private void mostraDescricao(Leilao leilao) {
        TextView descricao = findViewById(R.id.lances_leilao_descricao);
        descricao.setText(leilao.getDescricao());
    }
}
