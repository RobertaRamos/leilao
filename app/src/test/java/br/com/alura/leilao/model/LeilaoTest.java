package br.com.alura.leilao.model;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.util.List;

import br.com.alura.leilao.exception.LanceMenorQueUltimoLanceException;
import br.com.alura.leilao.exception.LanceSeguidoDoMesmoUsuarioException;
import br.com.alura.leilao.exception.UsuarioJaDeuCincoLancesException;

public class LeilaoTest {

    public static final double DELTA = 0.0001;
    private final Leilao CONSOLE = new Leilao("Console");
    private final Usuario ROBERTA = new Usuario("Roberta");

//    @Rule
//    public ExpectedException exception = ExpectedException.none();

    @Test
    public void deve_DevolveDescricao_QuandoRecebeDescricao() {
        // Executar ação esperada
        String descricaoDevolvida = CONSOLE.getDescricao();

        // Testar resultado esperado
        //assertEquals("Console", descricaoDevolvida);
        assertThat(descricaoDevolvida, is(equalTo("Console")));

    }
    // técnicas para nomeação dos métodos
    // [Nome do método][Estado de teste][Resultado esperado]
    // [deve][resultado esperado][estado de teste]

    @Test
    public void deve_DevolveMaiorLance_QuandoRecebeApenasUmLance() {
        CONSOLE.propoe(new Lance(ROBERTA, 200.0));

        Double maiorLanceDevolvidoDoConsole = CONSOLE.getMaiorLance();

        //assertEquals(200.0, maiorLanceDevolvidoDoConsole, DELTA);
        assertThat(maiorLanceDevolvidoDoConsole, closeTo(200.0, DELTA));
    }

    @Test
    public void deve_DevolveMaiorLance_QuandoRecebeMaisDeUmLanceEmOrdemCrescente() {
        CONSOLE.propoe(new Lance(ROBERTA, 100.0));
        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 300.0));

        Double maiorLanceDoComputador = CONSOLE.getMaiorLance();
        System.out.println(maiorLanceDoComputador);

        assertEquals(300.0, maiorLanceDoComputador, DELTA);
    }

//    @Test
//    public void deve_DevolveMaiorLance_QuandoRecebeMaisDeUmLanceEmOrdemDecrescente() {
//        CONSOLE.propoe(new Lance(ROBERTA, 15000.0));
//        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 10000.0));
//
//        Double maiorLanceDevolvidoDocarro = CONSOLE.getMaiorLance(    );
//
//        assertEquals(15000, maiorLanceDevolvidoDocarro, 0.001);
//
//    }

    @Test
    public void deve_DevolveMenorLance_QuandoRecebeApenasUmLance() {
        CONSOLE.propoe(new Lance(ROBERTA, 150.0));

        Double menorLanceDevolvido = CONSOLE.getMenorLance();

        assertEquals(150.0, menorLanceDevolvido, DELTA);
    }

    @Test
    public void deve_DevolveMenorLance_QuandoRecebeMaisDeUmLanceEmOrdemCrescente(){
        CONSOLE.propoe(new Lance(ROBERTA, 100.0));
        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 250.0));

        Double menorLanceDevolvidoDoComsole = CONSOLE.getMenorLance();
        System.out.println(menorLanceDevolvidoDoComsole);

        assertEquals(100.0, menorLanceDevolvidoDoComsole, DELTA);
    }

//    @Test
//    public void deve_DevolveMenorLance_QuandoRecebeMaisDeUmLanceEmOrdemDecrescente(){
//        CONSOLE.propoe(new Lance(ROBERTA, 12000.0));
//        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 9000.0));
//
//        Double menorLanceDevolvidoDoCarro = CONSOLE.getMenorLance();
//        System.out.println(menorLanceDevolvidoDoCarro);
//
//        assertEquals(9000.0, menorLanceDevolvidoDoCarro, DELTA);
//
//    }

    //test driven development(TDD)
    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeExatosTresLances(){
        CONSOLE.propoe(new Lance(ROBERTA, 200.0));
        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 300.0));
        CONSOLE.propoe(new Lance(ROBERTA, 400.0));

        List<Lance> tresMaioresLancesDevolvidos = CONSOLE.tresMaioresLances();

        //assertEquals(3, tresMaioresLancesDevolvidos.size());
       // assertThat(tresMaioresLancesDevolvidos, hasSize(equalTo(3)));
        //assertEquals(400.0, tresMaioresLancesDevolvidos.get(0).getValor(), DELTA);
        //assertThat(tresMaioresLancesDevolvidos, hasItem(new Lance(ROBERTA, 400.0)));
//        assertEquals(300.0, tresMaioresLancesDevolvidos.get(1).getValor(), DELTA);
//        assertEquals(200.0, tresMaioresLancesDevolvidos.get(2).getValor(), DELTA);
//        assertThat(tresMaioresLancesDevolvidos, contains(
//                new Lance(ROBERTA, 400.0),
//                new Lance(new Usuario("Eriberto"), 300.0),
//                new Lance(ROBERTA, 200.0)));

        assertThat(tresMaioresLancesDevolvidos, both(hasSize(3)).and(contains(
                new Lance(ROBERTA, 400.0),
                new Lance(new Usuario("Eriberto"), 300.0),
                new Lance(ROBERTA, 200.0))));


    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoNaoRecebeLances(){
        List<Lance> tresMaioresLancesDevolvidos = CONSOLE.tresMaioresLances();

        assertEquals(0, tresMaioresLancesDevolvidos.size());

    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeApenasUmLance(){
        CONSOLE.propoe(new Lance(ROBERTA, 200.0));

        List<Lance> tresMaioresLaancesDevolvidos = CONSOLE.tresMaioresLances();

        assertEquals(1, tresMaioresLaancesDevolvidos.size());
        assertEquals(200.0, tresMaioresLaancesDevolvidos.get(0).getValor(), DELTA);
    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeApenasDoisLances(){
        CONSOLE.propoe(new Lance(ROBERTA, 200.0));
        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 400.0));

        List<Lance> tresMaioresLancesDevolvidos = CONSOLE.tresMaioresLances();

        assertEquals(2, tresMaioresLancesDevolvidos.size());
        assertEquals(400.0, tresMaioresLancesDevolvidos.get(0).getValor(), DELTA);
        assertEquals(200.0, tresMaioresLancesDevolvidos.get(1).getValor(), DELTA);
    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoReceberApenasQuatroLances(){
        final Usuario ERIBERTO = new Usuario("Eriberto");
        CONSOLE.propoe(new Lance(ROBERTA, 300.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 400.0));
        CONSOLE.propoe(new Lance(ROBERTA, 500.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 600.0));

        List<Lance> tresLancesDevolvidos = CONSOLE.tresMaioresLances();

        assertEquals(3, tresLancesDevolvidos.size());
        assertEquals(600.0, tresLancesDevolvidos.get(0).getValor(), DELTA);
        assertEquals(500.0, tresLancesDevolvidos.get(1).getValor(), DELTA);
        assertEquals(400.0, tresLancesDevolvidos.get(2).getValor(), DELTA);

    }
    @Test
    public void deve_DevolverTresMaioresLances_QuandoReceberApenasCincoLances(){
        final Usuario ERIBERTO = new Usuario("Eriberto");
        CONSOLE.propoe(new Lance(ROBERTA, 300.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 400.0));
        CONSOLE.propoe(new Lance(ROBERTA, 500.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 600.0));
        CONSOLE.propoe(new Lance(ROBERTA, 900.0));

        List<Lance> tresMaioresLancesDevolvidos = CONSOLE.tresMaioresLances();

        assertEquals(3, tresMaioresLancesDevolvidos.size());
        assertEquals(900.0, tresMaioresLancesDevolvidos.get(0).getValor(), DELTA);
        assertEquals(600.0, tresMaioresLancesDevolvidos.get(1).getValor(), DELTA);
        assertEquals(500.0, tresMaioresLancesDevolvidos.get(2).getValor(), DELTA);

    }

    @Test
    public void deve_DevolverValorZeroParaMaiorLance_QuandoNaoTiverLance(){
        Double maiorLanceDevolvido = CONSOLE.getMaiorLance();

        assertEquals(0.0, maiorLanceDevolvido, DELTA);
    }
    @Test
    public void deve_DevolverValorZeroParaMenorLance_QuandoNaoTiverLance(){
        Double menorLanceDevolvido = CONSOLE.getMenorLance();

        assertEquals(0.0, menorLanceDevolvido, DELTA);
    }

    @Test(expected = LanceMenorQueUltimoLanceException.class)
    public void naoDeve_AdicionarLance_QuandoForMenorQueOMaiorLance(){
        //exception.expect(LanceMenorQueUltimoLanceException.class);
//        exception.expect(RuntimeException.class);
//        exception.expectMessage("Lance foi menor que maior lance");
        CONSOLE.propoe(new Lance(ROBERTA, 500.0));
        CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 400.0));
//        try {
//            CONSOLE.propoe(new Lance(new Usuario("Eriberto"), 400.0));
//            fail("Era esperada uma RuntimeException");
//        }catch (RuntimeException exception){
//            assertEquals("Lance foi menor que maior lance", exception.getMessage());
//        }


//        int quantidadeLancesDevolvida = CONSOLE.quantidadeLances();
//
//        assertEquals(1, quantidadeLancesDevolvida, DELTA);
    }

    @Test(expected = LanceSeguidoDoMesmoUsuarioException.class)
    public void naoDeve_AdicionarLance_QuandoForMesmoUsuarioDoUltimoLance(){
        //exception.expect(LanceSeguidoDoMesmoUsuarioException.class);
//        exception.expect(RuntimeException.class);
//        exception.expectMessage("Mesmo usuario do ultimo lance");
        CONSOLE.propoe(new Lance(ROBERTA, 600.0));
        CONSOLE.propoe(new Lance(ROBERTA, 700.0));
//        try {
//            CONSOLE.propoe(new Lance(ROBERTA, 700.0));
//            fail("Era esperada uma RuntimeException");
//        }catch (RuntimeException exception){
//            assertEquals("Mesmo usuario do ultimo lance", exception.getMessage());
//        }


//        int quantidadeLancesDevolvidos = CONSOLE.quantidadeLances();
//
//        assertEquals(1, quantidadeLancesDevolvidos);

    }

    @Test(expected = UsuarioJaDeuCincoLancesException.class)
    public void naoDeve_AdicionarLance_QuandoUsuarioDerCincoLances(){
        //xception.expect(UsuarioJaDeuCincoLancesException.class);
//        exception.expect(RuntimeException.class);
//        exception.expectMessage("Usuario ja deu cinco lances");
        final Usuario ERIBERTO = new Usuario("Eriberto");
        CONSOLE.propoe(new Lance(ROBERTA, 100.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 200.0));
        CONSOLE.propoe(new Lance(ROBERTA, 300.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 400.0));
        CONSOLE.propoe(new Lance(ROBERTA, 500.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 600.0));
        CONSOLE.propoe(new Lance(ROBERTA, 700.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 800.0));
        CONSOLE.propoe(new Lance(ROBERTA, 900.0));
        CONSOLE.propoe(new Lance(ERIBERTO, 1000.0));
        CONSOLE.propoe(new Lance(ROBERTA, 1100.0));
//        try {
//            CONSOLE.propoe(new Lance(ROBERTA, 1100.0));
//            fail("Era esperada uma RuntimeException");
//        }catch (RuntimeException exception){
//            assertEquals("Usuario ja deu cinco lances", exception.getMessage());
//        }

//        CONSOLE.propoe(new Lance(ERIBERTO, 1200.0));
//
//        int quantidadeLancesDevolvidos = CONSOLE.quantidadeLances();
//
//        assertEquals(10, quantidadeLancesDevolvidos);
    }
}