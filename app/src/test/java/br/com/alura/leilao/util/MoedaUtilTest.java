package br.com.alura.leilao.util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.hamcrest.MatcherAssert;
import org.junit.Test;


public class MoedaUtilTest {

    @Test
    public void Deve_devolverFormatoMoedaBrasileiro_QunadoRecebeUmLance() {


        String formataMoedaParaBrasileiro = MoedaUtil.formataMoedaParaBrasileiro(200.0);
        String formataMoedaParaBrasileiro1Teste = MoedaUtil.formataMoedaParaBrasileiro(500.0);

        assertEquals("R$ 200,00", formataMoedaParaBrasileiro);
        assertEquals("R$ 500,00",  formataMoedaParaBrasileiro1Teste);
        assertThat(formataMoedaParaBrasileiro, is(equalTo("R$ 200,00")));
    }
}